<div class="card shadow mb-4">
	<div class="card-header py-3"><?php echo empty($customer->first_name) ? 'Nuevo Cliente' : 'Editar Cliente'; ?></div>
	<div class="card-body">
		<?php if (validation_errors()) { ?>
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
				<?php echo validation_errors('<li>', '</li>'); ?>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		<?php } ?>

		<?= form_open(); ?>
		<div class="accordion" id="accordionExample">
			<div class="card">
				<div class="card-header" id="headingOne">
					<h2 class="mb-0">
						<button class="btn btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							NUEVO CLIENTE:
						</button>
					</h2>
				</div>

				<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
					<div class="card-body">
						<div class="form-row">
							<div class="form-group col-md-3">
								<label class="small mb-1" for="ci">Ingresar CI</label>
								<input class="form-control" id="ci" type="text" name="ci" value="<?php echo set_value('ci', $this->input->post('ci') ? $this->input->post('ci') : $customer->ci); ?>">
							</div>
							<div class="form-group col-md-3">
								<label class="small mb-1" for="first_name">Ingresar Nombre</label>
								<input class="form-control" id="first_name" style="text-transform:uppercase" type="text" name="first_name" value="<?php echo set_value('first_name', $this->input->post('first_name') ? $this->input->post('first_name') : $customer->first_name); ?>">
							</div>
							<div class="form-group col-md-3">
								<label class="small mb-1" for="last_name">Ingresar Apellidos</label>
								<input class="form-control" id="last_name" style="text-transform:uppercase" type="text" name="last_name" value="<?php echo set_value('last_name', $this->input->post('last_name') ? $this->input->post('last_name') : $customer->last_name); ?>">
							</div>
							<div class="form-group col-md-3">
								<label class="small mb-1" for="gender">Seleccionar Género</label>
								<select class="form-control" id="gender" name="gender">

									<?php if ($customer->gender == 'none') : ?>
										<option value="" selected>Seleccionar género</option>
									<?php endif ?>

									<option value="masculino" <?php if ($customer->gender == 'masculino') echo "selected" ?>>
										masculino
									</option>
									<option value="femenino" <?php if ($customer->gender == 'femenino') echo "selected" ?>>
										femenino
									</option>
								</select>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
								<label class="small mb-1" for="address">Ingresar dirección</label>
								<input class="form-control" id="address" type="text" name="address" value="<?php echo set_value('address', $this->input->post('address') ? $this->input->post('address') : $customer->address); ?>">
							</div>
							<div class="form-group col-md-4">
								<label class="small mb-1" for="mobile">Ingresar celular</label>
								<input class="form-control" id="mobile" type="text" name="mobile" value="<?php echo set_value('mobile', $this->input->post('mobile') ? $this->input->post('mobile') : $customer->mobile); ?>">
							</div>
							<div class="form-group col-md-4">
								<label class="small mb-1" for="phone">Ingresar Teléfono</label>
								<input class="form-control" id="phone" type="text" name="phone" value="<?php echo set_value('phone', $this->input->post('phone') ? $this->input->post('phone') : $customer->phone); ?>">
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-4">
								<label class="small mb-1" for="business_name">Ingresar razon social</label>
								<input class="form-control" id="business_name" type="text" name="business_name" value="<?php echo set_value('business_name', $this->input->post('business_name') ? $this->input->post('business_name') : $customer->business_name); ?>">
							</div>
							<div class="form-group col-md-4">
								<label class="small mb-1" for="nit">Ingresar NIT</label>
								<input class="form-control" id="nit" type="text" name="nit" value="<?php echo set_value('nit', $this->input->post('nit') ? $this->input->post('nit') : $customer->nit); ?>">
							</div>
							<div class="form-group col-md-4">
								<label class="small mb-1" for="company">Ingresar empresa</label>
								<input class="form-control" id="company" type="text" name="company" value="<?php echo set_value('company', $this->input->post('company') ? $this->input->post('company') : $customer->company); ?>">
							</div>
							<div class="form-group col-md-4" hidden>
								<input class="form-control" hidden readonly=true id="user_id" type="number" name="user_id" value="<?php echo set_value('user_id', $this->input->post('user_id') ? $this->input->post('user_id') : $customer->user_id); ?>">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingTwo">
					<h2 class="mb-0">
						<button class="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							DATOS DEL CONYUGUE Y/O CO-DEUDOR:
						</button>
					</h2>
				</div>
				<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
					<div class="card-body">
						<div class="form-row">
							<div class="form-group col-md-3">
								<label class="small mb-1" for="ci_conyugue">Ingresar CI</label>
								<input class="form-control" id="ci" type="text" name="ci_conyugue" value="<?php echo set_value('ci_conyugue', $this->input->post('ci_conyugue') ? $this->input->post('ci_conyugue') : $customer->ci_conyugue); ?>">
							</div>
							<div class="form-group col-md-3">
								<label class="small mb-1" for="first_name_conyugue">Ingresar Nombre</label>
								<input class="form-control" id="first_name_conyugue" style="text-transform:uppercase" type="text" name="first_name_conyugue" value="<?php echo set_value('first_name_conyugue', $this->input->post('first_name_conyugue') ? $this->input->post('first_name_conyugue') : $customer->first_name_conyugue); ?>">
							</div>
							<div class="form-group col-md-3">
								<label class="small mb-1" for="last_name_conyugue">Ingresar Apellidos</label>
								<input class="form-control" id="last_name_conyugue" style="text-transform:uppercase" type="text" name="last_name_conyugue" value="<?php echo set_value('last_name_conyugue', $this->input->post('last_name_conyugue') ? $this->input->post('last_name_conyugue') : $customer->last_name_conyugue); ?>">
							</div>
							<div class="form-group col-md-3">
								<label class="small mb-1" for="gender_conyugue">Seleccionar Género</label>
								<select class="form-control" id="gender_conyugue" name="gender_conyugue">

									<?php if ($customer->gender_conyugue == 'none') : ?>
										<option value="" selected>Seleccionar género</option>
									<?php endif ?>

									<option value="masculino" <?php if ($customer->gender_conyugue == 'masculino') echo "selected" ?>>
										masculino
									</option>
									<option value="femenino" <?php if ($customer->gender_conyugue == 'femenino') echo "selected" ?>>
										femenino
									</option>
								</select>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
								<label class="small mb-1" for="address_conyugue">Ingresar dirección</label>
								<input class="form-control" id="address_conyugue" type="text" name="address_conyugue" value="<?php echo set_value('address_conyugue', $this->input->post('address_conyugue') ? $this->input->post('address_conyugue') : $customer->address_conyugue); ?>">
							</div>
							<div class="form-group col-md-4">
								<label class="small mb-1" for="mobile_conyugue">Ingresar celular</label>
								<input class="form-control" id="mobile_conyugue" type="text" name="mobile_conyugue" value="<?php echo set_value('mobile_conyugue', $this->input->post('mobile_conyugue') ? $this->input->post('mobile_conyugue') : $customer->mobile_conyugue); ?>">
							</div>
							<div class="form-group col-md-4">
								<label class="small mb-1" for="phone_conyugue">Ingresar Teléfono</label>
								<input class="form-control" id="phone_conyugue" type="text" name="phone_conyugue" value="<?php echo set_value('phone_conyugue', $this->input->post('phone_conyugue') ? $this->input->post('phone_conyugue') : $customer->phone_conyugue); ?>">
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-4">
								<label class="small mb-1" for="business_name_conyugue">Ingresar razon social</label>
								<input class="form-control" id="business_name_conyugue" type="text" name="business_name_conyugue" value="<?php echo set_value('business_name_conyugue', $this->input->post('business_name_conyugue') ? $this->input->post('business_name_conyugue') : $customer->business_name_conyugue); ?>">
							</div>
							<div class="form-group col-md-4">
								<label class="small mb-1" for="nit_conyugue">Ingresar NIT_conyugue</label>
								<input class="form-control" id="nit_conyugue" type="text" name="nit_conyugue" value="<?php echo set_value('nit_conyugue', $this->input->post('nit_conyugue') ? $this->input->post('nit_conyugue') : $customer->nit_conyugue); ?>">
							</div>
							<div class="form-group col-md-4">
								<label class="small mb-1" for="company_conyugue">Ingresar empresa</label>
								<input class="form-control" id="company_conyugue" type="text" name="company_conyugue" value="<?php echo set_value('company_conyugue', $this->input->post('company_conyugue') ? $this->input->post('company_conyugue') : $customer->company_conyugue); ?>">
							</div>
							<div class="form-group col-md-4" hidden>
								<input class="form-control" hidden readonly=true id="user_id" type="number" name="user_id" value="<?php echo set_value('user_id', $this->input->post('user_id') ? $this->input->post('user_id') : $customer->user_id); ?>">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingThree">
					<h2 class="mb-0">
						<button class="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							REFERENCIAS PERSONALES, PERSONALES Y COMERCIALES:
						</button>
					</h2>
				</div>
				<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
					<div class="card-body">
						<div class="form-row">
							<div class="form-group col-md-12">
								<label class="small mb-1" for="staff">Personal</label></label>
								<input class="form-control" id="staff" type="text" name="staff" value="<?php echo set_value('staff', $this->input->post('staff') ? $this->input->post('staff') : $customer->staff); ?>">
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-12">
								<label class="small mb-1" for="commercial">Comercial</label>
								<input class="form-control" id="commercial" type="text" name="commercial" value="<?php echo set_value('commercial', $this->input->post('commercial') ? $this->input->post('commercial') : $customer->commercial); ?>">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingFour">
					<h2 class="mb-0">
						<button class="btn  btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
							DATOS DEL NEGOCIO:
						</button>
					</h2>
				</div>
				<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
					<div class="card-body">
						<div class="accordion" id="accordion">
							<div class="card">
								<div class="card-header" id="headingOne">
									<h2 class="mb-0">
										<button class="btn btn-block text-left" type="button" data-toggle="collapse" data-target="#actividad" aria-expanded="true" aria-controls="actividad">
											Actividad
										</button>
									</h2>
								</div>

								<div id="actividad" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
									<div class="card-body">
										<div class="form-row">
											<div class="form-group col-md-4">
												<label class="small mb-1" for="activity">Actividad</label></label>
												<input class="form-control" id="activity" type="text" name="activity" value="<?php echo set_value('activity', $this->input->post('activity') ? $this->input->post('activity') : $customer->activity); ?>">
											</div>
											<div class="form-group col-md-4">
												<label class="small mb-1" for="specific_activity">Actividad Especifica</label></label>
												<input class="form-control" id="specific_activity" type="text" name="specific_activity" value="<?php echo set_value('specific_activity', $this->input->post('specific_activity') ? $this->input->post('specific_activity') : $customer->specific_activity); ?>">
											</div>
											<div class="form-group col-md-4">
												<label class="small mb-1" for="experience_in_the_field">Experiencia en el Rubro</label></label>
												<input class="form-control" id="experience_in_the_field" type="text" name="experience_in_the_field" value="<?php echo set_value('experience_in_the_field', $this->input->post('experience_in_the_field') ? $this->input->post('experience_in_the_field') : $customer->experience_in_the_field); ?>">
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-4">
												<label class="small mb-1" for="seniority_in_business">Antiguedad en el Negocio</label></label>
												<input class="form-control" id="seniority_in_business" type="text" name="seniority_in_business" value="<?php echo set_value('seniority_in_business', $this->input->post('seniority_in_business') ? $this->input->post('seniority_in_business') : $customer->seniority_in_business); ?>">
											</div>
											<div class="form-group col-md-4">
												<label class="small mb-1" for="age_at_current_location">Antigueda de la Ubicaion Actual</label></label>
												<input class="form-control" id="age_at_current_location" type="text" name="age_at_current_location" value="<?php echo set_value('age_at_current_location', $this->input->post('age_at_current_location') ? $this->input->post('age_at_current_location') : $customer->age_at_current_location); ?>">
											</div>
											<div class="form-group col-md-4">
												<label class="small mb-1" for="ownership_of_the_position_or_place">Propiedad del Puesto o Lugar</label></label>
												<input class="form-control" id="ownership_of_the_position_or_place" type="text" name="ownership_of_the_position_or_place" value="<?php echo set_value('ownership_of_the_position_or_place', $this->input->post('ownership_of_the_position_or_place') ? $this->input->post('ownership_of_the_position_or_place') : $customer->ownership_of_the_position_or_place); ?>">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="headingTwo">
									<h2 class="mb-0">
										<button class="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#actividadDos" aria-expanded="false" aria-controls="actividadDos">
											Actividad #2
										</button>
									</h2>
								</div>
								<div id="actividadDos" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
									<div class="card-body">
									<div class="form-row">
											<div class="form-group col-md-4">
												<label class="small mb-1" for="activity_two">Actividad</label></label>
												<input class="form-control" id="activity_two" type="text" name="activity_two" value="<?php echo set_value('activity_two', $this->input->post('activity_two') ? $this->input->post('activity_two') : $customer->activity_two); ?>">
											</div>
											<div class="form-group col-md-4">
												<label class="small mb-1" for="specific_activity_two">Actividad Especifica</label></label>
												<input class="form-control" id="specific_activity_two" type="text" name="specific_activity_two" value="<?php echo set_value('specific_activity_two', $this->input->post('specific_activity_two') ? $this->input->post('specific_activity_two') : $customer->specific_activity_two); ?>">
											</div>
											<div class="form-group col-md-4">
												<label class="small mb-1" for="experience_in_the_field_two">Experiencia en el Rubro</label></label>
												<input class="form-control" id="experience_in_the_field_two" type="text" name="experience_in_the_field_two" value="<?php echo set_value('experience_in_the_field_two', $this->input->post('experience_in_the_field_two') ? $this->input->post('experience_in_the_field_two') : $customer->experience_in_the_field_two); ?>">
											</div>
										</div>
										<div class="form-row">
											<div class="form-group col-md-4">
												<label class="small mb-1" for="seniority_in_business_two">Antiguedad en el Negocio</label></label>
												<input class="form-control" id="seniority_in_business_two" type="text" name="seniority_in_business_two" value="<?php echo set_value('seniority_in_business_two', $this->input->post('seniority_in_business_two') ? $this->input->post('seniority_in_business_two') : $customer->seniority_in_business_two); ?>">
											</div>
											<div class="form-group col-md-4">
												<label class="small mb-1" for="age_at_current_location_two">Antigueda de la Ubicaion Actual</label></label>
												<input class="form-control" id="age_at_current_location_two" type="text" name="age_at_current_location_two" value="<?php echo set_value('age_at_current_location_two', $this->input->post('age_at_current_location_two') ? $this->input->post('age_at_current_location_two') : $customer->age_at_current_location_two); ?>">
											</div>
											<div class="form-group col-md-4">
												<label class="small mb-1" for="ownership_of_the_position_or_place_two">Propiedad del Puesto o Lugar</label></label>
												<input class="form-control" id="ownership_of_the_position_or_place_two" type="text" name="ownership_of_the_position_or_place_two" value="<?php echo set_value('ownership_of_the_position_or_place_two', $this->input->post('ownership_of_the_position_or_place_two') ? $this->input->post('ownership_of_the_position_or_place_two') : $customer->ownership_of_the_position_or_place_two); ?>">
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="float-right">
			<a href="<?php echo site_url('admin/customers/'); ?>" class="btn btn-dark">Cancelar</a>
			<button class="btn btn-primary" type="submit">Guardar</button>
		</div>
		<?= form_close() ?>
	</div>
</div>
